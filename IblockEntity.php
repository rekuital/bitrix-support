<?php

use Bitrix\Iblock\Elements;
use Bitrix\Iblock\Iblock;
use Bitrix\Iblock\IblockSiteTable;
use Bitrix\Iblock\IblockTable;
use Bitrix\Main\Context;
use Bitrix\Main\Entity\Query;
use Bitrix\Main\Entity\Query\Filter\ConditionTree;
use Bitrix\Main\Entity\ReferenceField;
use Bitrix\Main\ObjectNotFoundException;
use Bitrix\Main\Text\StringHelper;

/**
 * @method static Elements\ElementExampleTable|string getIblockExampleEntity()
 * @method static int getIblockIdExample()
 *
 */
class IblockEntity
{
    public const EXAMPLE = 'Example';

    protected const REGEX_IBLOCK_ENTITY = '/getIblock(.*)Entity/';
    protected const REGEX_IBLOCK_ID = '/getIblockId(.*)/';
    protected const DEFAULT_SITE_ID = 's1';

    protected static array $cacheIblockId = [];

    /**
     * @throws \Bitrix\Main\SystemException
     * @throws \Bitrix\Main\ArgumentException
     */
    public static function __callStatic($name, $arguments)
    {
        if (preg_match(self::REGEX_IBLOCK_ID, $name) === 1) {

            $apiCode = preg_replace(self::REGEX_IBLOCK_ID, '$1', $name);
            $iblockEntity = self::getIblockIdByApiCode($apiCode);
            return $iblockEntity;

        } elseif (preg_match(self::REGEX_IBLOCK_ENTITY, $name) === 1) {

            $apiCode = preg_replace(self::REGEX_IBLOCK_ENTITY, '$1', $name);
            $iblockEntity = Iblock::wakeUp(self::getIblockIdByApiCode($apiCode))->getEntityDataClass();
            return $iblockEntity;

        } else {
            throw new Exception('Вызов не определенного метода');
        }
    }

    /**
     * @param string $nameEntity
     * @return \Bitrix\Iblock\ORM\CommonElementTable|string
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\SystemException
     */
    public static function getElementTableBy(string $nameEntity)
    {
        $iblockEntity = Iblock::wakeUp(self::getIblockIdByApiCode($nameEntity))->getEntityDataClass();
        return $iblockEntity;
    }

    /**
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\SystemException
     */
    public static function getIblockIdByApiCode($apiCode): int
    {
        if (isset(self::$cacheIblockId[$apiCode])) { return self::$cacheIblockId[$apiCode]; }

        $siteId = Context::getCurrent()->getSite();

        $filter = Query::filter()
            ->logic(ConditionTree::LOGIC_AND)
            ->where('IBLOCK_LID.SITE_ID', $siteId)
            ->addCondition(
                Query::filter()
                    ->logic(ConditionTree::LOGIC_OR)
                    ->where('API_CODE', $apiCode)
                    ->where('API_CODE', $apiCode.StringHelper::snake2camel($siteId))
            );

        $rsIblock = IblockTable::query()
            ->setCacheTtl(3600)
            ->cacheJoins(true)
            ->where(
                $filter
            )
            ->registerRuntimeField(
                'IBLOCK_LID',
                new ReferenceField(
                    'IBLOCK_LID',
                    IblockSiteTable::class,
                    ['this.ID' => 'ref.IBLOCK_ID']
                )
            )
            ->setSelect(['ID'])
            ->setOrder(self::getOrder());

        $iblock = $rsIblock->fetchObject();

        if (!$iblock) {
            throw new Exception("Инфоблок по коду $apiCode не найден");
        }

        self::$cacheIblockId[$apiCode] = $iblock->getId();

        return self::$cacheIblockId[$apiCode];
    }

    protected static function getOrder(): array
    {
        $order = [
            'API_CODE' => Context::getCurrent()->getSite() === self::DEFAULT_SITE_ID ? 'ASC' : 'DESC'
        ];

        return $order;
    }

}
