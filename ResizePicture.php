<?php

use CFile;

/**
 * @method static ResizePicture loadFileForRetina($fileId)
 * @method static ResizePicture loadFileForDesktop($fileId)
 * @method static ResizePicture loadFileForTablet($fileId)
 * @method static ResizePicture loadFileForMobile($fileId)
 * @method static ResizePicture loadFileForAll($fileId)
 *
 * @method ResizePicture addFileForRetina($fileId)
 * @method ResizePicture addFileForDesktop($fileId)
 * @method ResizePicture addFileForTablet($fileId)
 * @method ResizePicture addFileForMobile($fileId)
 * @method ResizePicture addFileForAll($fileId)
 *
 * @method ResizePicture addSchemeByRetina(int $width, int $height)
 * @method ResizePicture addSchemeByDesktop(int $width, int $height)
 * @method ResizePicture addSchemeByTablet(int $width, int $height)
 * @method ResizePicture addSchemeByMobile(int $width, int $height)
 * @method ResizePicture addSchemeByAll(int $width, int $height)
 *
 * @method string getSrcByRetina()
 * @method string getSrcByDesktop()
 * @method string getSrcByTablet()
 * @method string getSrcByMobile()
 * @method string getSrcByAll()
 */
class ResizePicture
{
    protected const WIDTH = 'width';
    protected const HEIGHT = 'height';

    public const SIZE_PROPORTIONAL = 999999;


    /**
     * @var array
     */
    protected array $fileIds;

    /**
     * @var array
     */
    protected array $scheme;

    /**
     * @var int
     */
    protected int $resizeType;

    /**
     * @var string
     */
    protected string $bySide;

    /**
     * @var array
     */
    protected array $srcFiles;

    public static function __callStatic($name, $arguments)
    {
        if (strpos($name, 'loadFileFor') === 0) {
            $that = new static(str_replace('loadFileFor', '', $name), (int)$arguments[0]);

            return $that;
        }

        return null;
    }

    public function __construct(string $type, int $fileIds)
    {
        $this->srcFiles = [];
        $this->fileIds = [];
        $this->scheme = [];
        $this->resizeType = BX_RESIZE_IMAGE_PROPORTIONAL;
        $this->bySide = 'max';

        $this->addFileFor($type, $fileIds);
    }

    public function __call($name, $arguments)
    {
        if (strpos($name, 'addSchemeBy') === 0) {
            return $this->addSchemeBy(str_replace('addSchemeBy', '', $name), $arguments[0], $arguments[1]);
        } elseif (strpos($name, 'getSrcBy') === 0) {
            return $this->getSrcBy(str_replace('getSrcBy', '', $name));
        } elseif (strpos($name, 'addFileFor') === 0) {
            return $this->addFileFor(str_replace('addFileFor', '', $name), (int)$arguments[0]);
        }
        return null;
    }

    protected function addSchemeBy(string $type, int $width, int $height): ResizePicture
    {
        $this->scheme[$type] = [self::WIDTH => $width, self::HEIGHT => $height];

        return $this;
    }

    protected function getSrcBy($type): string
    {
        return $this->srcFiles[$type];
    }

    protected function addFileFor(string $type, int $fileId): self
    {
        $this->fileIds[$type] = $fileId;

        return $this;
    }

    public function getAllPictures(): array
    {
        return array_change_key_case(array_filter($this->srcFiles), CASE_LOWER);
    }

    public function setResizeType(int $resizeType): ResizePicture
    {
        $this->resizeType = $resizeType;

        return $this;
    }

    public function setByMinSide(): ResizePicture
    {
        $this->bySide = 'min';

        return $this;
    }

    public function setByMaxSide(): ResizePicture
    {
        $this->bySide = 'max';

        return $this;
    }

    protected static function calculateSize($origSize, $schemeSize): array
    {
        if ($schemeSize['width'] === self::SIZE_PROPORTIONAL) {
            return [
                'width' => round($origSize['WIDTH'] / ($origSize['HEIGHT'] / $schemeSize['height'])),
                'height' => $schemeSize['height'],
            ];
        }
        if ($schemeSize['height'] === self::SIZE_PROPORTIONAL) {
            return [
                'width' => $schemeSize['width'],
                'height' => round($origSize['HEIGHT'] / ($origSize['WIDTH'] / $schemeSize['width'])),
            ];
        }

        if ($origSize['WIDTH'] > $origSize['HEIGHT']) {
            $ratio = $origSize['WIDTH'] / $schemeSize['width'];
        } else {
            $ratio = $origSize['HEIGHT'] / $schemeSize['height'];
        }

        return [
            'width' => $origSize['WIDTH'] > $origSize['HEIGHT'] ? $schemeSize['width'] : round($origSize['WIDTH'] / $ratio),
            'height' => $origSize['WIDTH'] > $origSize['HEIGHT'] ? round($origSize['HEIGHT'] / $ratio) : $schemeSize['height'],
        ];
    }

    public function resize(): ResizePicture
    {
        $this->srcFiles = [];

        $files = [];

        $fileIds = array_values(array_unique($this->fileIds));
        if (!$fileIds) { return $this; }

        $rsFile = CFile::GetList([], ['@ID' => implode(',', $fileIds)]);
        while ($ob = $rsFile->Fetch()) {
            $ob['SRC'] = CFile::GetFileSRC($ob);
            $files[$ob['ID']] = $ob;

        }

        if ($this->fileIds) {
            foreach ($this->scheme as $type => $size) {
                $this->srcFiles[$type] = '';

                $file = $files[$this->fileIds[$type] ?: $this->fileIds['All']];

                if (!$file) { continue; }

                $src = CFile::ResizeImageGet(
                    $file,
                    self::calculateSize($file, $size),
                    $this->resizeType
                )['src'];
                $this->srcFiles[$type] = $src ?: '';
            }
        }

        return $this;
    }
}
